<?php

namespace Drupal\csv_serialization;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Register the CSV format.
 *
 * @internal
 */
class CsvSerializationServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('http_middleware.negotiation')
      ->addMethodCall('registerFormat', [
        'csv',
        ['text/csv'],
      ]);
  }

}
